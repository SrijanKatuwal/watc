from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from . models import Timing
from . choices import *
from django.core.validators import MaxValueValidator, MinValueValidator

class Signup_form(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')

class Login_form(forms.Form):
    username = forms.CharField(max_length=32)
    password = forms.CharField(max_length=32, widget=forms.PasswordInput)

class Add_web_form(forms.Form):
    web_address = forms.CharField(max_length=64)


class Timing_form(forms.Form):
    time_group = forms.ChoiceField(choices=TIME_CHOICES, required=True)
    time_value = forms.IntegerField(required=True)