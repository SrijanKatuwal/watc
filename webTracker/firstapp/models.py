from django.db import models
from . choices import *


# Create your models here.
class Website(models.Model):
    username = models.CharField(max_length=32)
    website_name = models.CharField(max_length=150)

    def __str__(self):
        return self.website_name
    
    
class Timing(models.Model):
    username = models.CharField(max_length=32)
    time_in_sec = models.IntegerField()
