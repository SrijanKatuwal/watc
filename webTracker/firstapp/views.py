from django.shortcuts import render, redirect
from . forms import Signup_form, Login_form, Add_web_form, Timing_form
from django.contrib.auth import login, authenticate, logout
from . models import Website, Timing
from django.contrib.auth.decorators import login_required
from . tasks import checkwebsite
from . views_func import website_exist, calculate_time, is_timing_selected


def signup(request):
    # for post request
    if request.method == 'POST':
        form = Signup_form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/home/')
    
    # for get request
    else:
        form = Signup_form()
    
    return render(request, 'signup.html', {'form': form})


def signin(request):
    # for post request
    if request.method == "POST":
        form = Login_form(request.POST)

        # block for valid form
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)

            # loging in if user exist
            if user is not None:
                login(request, user)
                return redirect('home')
            else:
                return render(request, 'login.html', {'error': "Wrong username or password", 'form': form})
    
    # for get request
    else:
        form = Login_form()
    
    return render(request, 'login.html', {'form': form})


@login_required(login_url='/signin/')
def home(request):
    web_list = Website.objects.filter(username=request.user.username)

    # for post request
    if request.method=="POST":
        form = Add_web_form(request.POST)

        # block for valid form
        if form.is_valid():
            web_name = form.cleaned_data.get('web_address')
            
            if checkwebsite(web_name) == 404:
                msg = "There is no such website"
                context = {'form': form, 'web_list': web_list, 'msg': msg}
                return render(request, 'home.html', context)
            else:
                if website_exist(request.user.username, web_name) == False:
                    obj = Website(username=request.user.username, website_name=web_name)
                    obj.save()
                else:
                    msg = "Website already exist"
                    context = {'form': form, 'web_list': web_list,'msg': msg}
                    return render(request, 'home.html', context)

        # block for logout    
        else:
            logout(request)
            return redirect('signin')

    # for get request
    else:
        form = Add_web_form()
        timing_form = Timing_form()

    context = {'form': form, 'web_list': web_list,}
    return render(request, 'home.html', context) 


@login_required(login_url='/signin/')
def time_select(request):
    timing_selected = is_timing_selected(request.user.username)
    timing_form = Timing_form()
    if timing_selected:
        timing_msg = "Timing already selected your can update the timing"
        context = {'timing_form': timing_form, 'timing_msg': timing_msg}

    if request.method=="POST":
        timing_form = Timing_form(request.POST)
        # block for valid timing form
        if timing_form.is_valid():
            time_group = timing_form.cleaned_data.get('time_group')
            time_value = timing_form.cleaned_data.get('time_value')

            # validating the input value of time 
            if time_value <=0:
                error = "Please input a positive value"
                context = {'timing_form': timing_form, 'error': error}
                return render(request, 'timing.html', context)
            else:
                time_in_sec = calculate_time(time_group, time_value)
                if timing_selected:
                    t = Timing.objects.get(username=request.user.username)
                    t.time_in_sec = time_in_sec
                    t.save()
                else:
                    obj = Timing(username=request.user.username, time_in_sec=time_in_sec)
                    obj.save()
    else:
        
        context = {'timing_form': timing_form}
    return render(request, 'timing.html', context)
