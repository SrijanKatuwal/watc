from . models import Website, Timing

def website_exist(username, url):
    try:
        Website.objects.get(username=username, website_name=url)
        return True
    except:
        return False

def calculate_time(group, value):
    if group == "Day":
        return value*24*60*60
    elif group == "Hours":
        return value*60*60
    else:
        return value*60


def is_timing_selected(username):
    try:
        Timing.objects.get(username=username)
        return True
    except:
        return False
