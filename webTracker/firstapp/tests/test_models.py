import pytest
from firstapp.models import Website, Timing

@pytest.mark.django_db
class TestModels:

    def test_website_create_object(self):
        obj = Website.objects.create(username="sri123jan", website_name="https://www.facebook.com/")
        assert obj.username == "sri123jan"
        assert obj.website_name == "https://www.facebook.com/"

    def test_timing_create_object(self):
        obj = Timing.objects.create(username="sri123jan", time_in_sec=400)
        assert obj.username == "sri123jan"
        assert obj.time_in_sec == 400

    def test_website_update_value_of_object(self):
        obj = Website.objects.create(username="sri123jan", website_name="https://www.facebook.com/")
        obj1 = Website.objects.get(username="sri123jan")
        obj1.username = "sri456jan"
        assert obj1.username == "sri456jan"
        assert obj1.website_name == "https://www.facebook.com/" 
    
    def test_timing_update_value_of_object(self):
        obj = Timing.objects.create(username="sri123jan", time_in_sec=400)
        obj1 = Timing.objects.get(username="sri123jan")
        obj1.username = "sri456jan"
        assert obj1.username == "sri456jan"
        assert obj1.time_in_sec == 400
        