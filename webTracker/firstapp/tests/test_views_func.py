import pytest
from firstapp.models import Website, Timing
from firstapp.views_func import website_exist, calculate_time, is_timing_selected

@pytest.mark.django_db
class TestFunc:
    
    def test_website_exist_with_non_existing_object(self):
        assert website_exist(username="sri123jan", url="https://stackoverflow.com") == False

    def test_calculate_time_day(self):
        assert calculate_time("Day", 2) == 172800
    
    def test_calculate_time_hour(self):
        assert calculate_time("Hours", 24) == 86400

    def test_calculate_time_minute(self):
        assert calculate_time("Minutes", 10) == 600

    def test_is_timing_selected_with_non_existing_object(self):
        assert is_timing_selected(username="sri123jan") == False