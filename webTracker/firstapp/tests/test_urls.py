from django.urls import resolve, reverse

class TestUrls:

    def test_home_url(self):
        path = reverse('home')
        assert resolve(path).view_name == 'home'

    def test_signup_url(self):
        path = reverse('signup')
        assert resolve(path).view_name == 'signup'

    def test_signin_url(self):
        path = reverse('signin')
        assert resolve(path).view_name == 'signin'
    
    def test_time_select_url(self):
        path = reverse('time_select')
        assert resolve(path).view_name == 'time_select'