from django.contrib import admin
from . models import Website, Timing

# Register your models here.
admin.site.register(Website)
admin.site.register(Timing)