from django.utils.translation import gettext as _

TIME_CHOICES = (
    ("Day", _("Day")),
    ("Hours", _("Hours")),
    ("Minutes", _("Minute")),
)
