from __future__ import absolute_import, unicode_literals
from celery import shared_task
import requests

@shared_task
def add(x, y):
    return x + y

@shared_task 
def checkwebsite(websiteurl):
    try:
        response = requests.get(websiteurl)
        return response.status_code
    except:
        return 404
    
